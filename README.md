# Owned Car App
This repo holds the code that fulfills the programming task set by White Rose Maths. The task was to create an API that manages people and the cars they own.


# Tech Stack
The Owned Car App was built with the following technologies:
- api-platform 2.6.4
- symfony 5.2
- php 8.0
- Docker

# Assumptions
1. A car can only be owned by one Person
2. A car entity cannot exist without a Person Owner
3. The User of the API is not a Person entity.
4. A User has CRUD operational access to Person and Car entities.

# Running the application
The following steps detail how to run the Owned Car Application locally:

```
// Clone the repo
git clone https://gitlab.com/vanilla.gorilla.limited/car-owner-app.git
// Get the Docker images
docker-compose pull 
// Start the containers
docker-compose up -d
// Create the database
docker-compose exec php bin/console doctrine:migrations:diff
docker-compose exec php bin/console doctrine:migrations:migrate
```

# Accessing the API
The API is accessible using Postman. 
The API is password protected using JWT tokens, only the auth directories are public accessible.
```
# api/config/packages/security.yaml
security:
    providers:
        app_user_provider:
            entity:
                class: App\Entity\User
                property: email
    firewalls:
        main:
            guard:
                authenticators:
                    - App\Security\JwtAuthenticator

    access_control:
        - { path: ^/auth, roles: PUBLIC_ACCESS }
        - { path: ^/docs, roles: PUBLIC_ACCESS }
        - { path: ^/, roles: ROLE_USER }
```
Therefore you must be a registered User to access the API. A User entity requires an email and a password.


A user can be registered using the *auth/register* POST api endpoint:
```
POST http://localhost/auth/register
Headers: Content-Type: application/json
Body - raw JSON: {
  "email": "test@example.com",
  "password": "123123",
}
```

A JWT Bearer token is accessed using the */auth/login* POST api endpoint:
```
POST http://localhost/auth/login
Headers: Content-Type: application/json
Body - raw JSON: {
  "email": "test@example.com",
  "password": "123123",
}
```

A successful login will return the following:
```
{
    "message": "success!",
    "token": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyIjoidGVzdEBleGFtcGxlLmNvbSIsImV4cCI6MTYyMDgxNjQ3OX0.wVWSox8zT8wcizPif6oSaZgBDlvB_8fALzpn4xFDkVU"
}
```

This bearer token can then be used to access the CRUD operations of the People and Cars entities.
```
GET http://localhost/people
Headers: Authorization: Bearer eyJ0eXA...
         Content-Type: application/json
```

# API Endpoints
Documentation of the API endpoint can be found at: https://localhost/docs
This is the generated swagger UI that has been given public access for this demo.

### Creating a Person Entity Example
```
POST http://localhost/people
Headers: Authorization: Bearer eyJ0eXA...
         Content-Type: application/json
Body - raw JSON: {
  "firstName": "Andrew",
  "lastName": "Smith",
  "dateOfBirth": "1967-05-11T19:46:36.438Z"
}
```

### Creating a Car Entity Example
```
POST http://localhost/cars
Headers: Authorization: Bearer eyJ0eXA...
         Content-Type: application/json
Body - raw JSON: {
  "make": "Ford",
  "model": "Focus",
  "registrationNumber": "ABC123",
  "owner": "\/people\/2"
}
```


# Limitations
1. The API asserts that the Car make and model are not blank. However they do not validate if the data is legitimate car make / model data.
2. The API asserts the the Person dateOfBirth is a date object, however it doesn't validate if the date of birth is in the past.
3. The JWT authorization uses the firebase/jwt plugin. However the access control is written by hand and is of the older 'Guardian' style. It would be better to refactor to use the lexik/LexikJWTAuthenticationBundle however I wanted a better understanding of how the access control worked.  

# Entity Special Mentions
### createdAt & updatedAt
The createdAt property is generated on a prePersist hook. Before the entity gets saved to the database, the prePersist hook is called to generate the createdAt date time of the time now. The createdAt is a datetime_immutable object, which means it cannot be updated once it is set.

The updatedAt property is generated from the preUpadte hook. Anytime the entity is updated, this hook gets called and updates the updatedAt property.

### Person
The age property of the person entity is a calculated entity requiring a normalization group on the get:
```
#[ApiResource(
  collectionOperations: [
      'get' => ['normalization_context' => ['groups' => 'get']],
      "post"
  ],  
)]
class Person
{
 ....
 /**
     * @Groups({"get"})
     */
    public function getAge(): ?int
    {
        $age = null;
        if($this->getDateOfBirth() != null)
        {
          $today  = new DateTime('today');
          $age = $this->getDateOfBirth()->diff($today)->y;
        }

        return $age;
    }
}
```

# Unit Tests
Unit tests have been written for both the Person and the Car entities. The unit tests are runnable with the following command:
```
docker-compose exec php bin/phpunit
```

# Development Process
The following steps were used to develop the application:
1. Download the api-platform-2.6.4.tar file and extract it
2. Init the docker containers
3. Install the following dependencies with the docker-compose exec php sh -c '' command
```
composer require make
composer require orm
composer require security
composer require firebase/php-jwt
composer require doctrine/annotations
composer require --dev alice
composer require --dev symfony/test-pack symfony/http-client justinrainbow/json-schema
```
4. Create the Person and Car entity classes using the make command:
```
docker-compose exec php bin/console make:entity --api-resource 
```
This enabled fast and accurate create of the CRUD endpoints for the entities.

5. Create the User
```
docker-compose exec php bin/console make:user
```
6. Implement the JWT authorization and update the security.yaml to handle access_control

7. Implement fixtures and unit tests

The process took several hours to complete. However a large portion of that time was learning how to implement the JWT authentication and implementing the unit tests.

# Deployment
The application is built with the api-platform and is therefore containerized. It is possible to deploy this to a hosting service with the docker-compose commands, however the .env files will need to be updated with the appropriate allowed hosts etc.
