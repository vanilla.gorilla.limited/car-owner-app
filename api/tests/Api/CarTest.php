<?php

namespace App\Tests;

use App\Entity\Car;
use App\Entity\Person;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Client;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;

class CarTest extends ApiTestCase
{
    private $token;
    private $clientWithCredentials;
    private $personID;

    use RefreshDatabaseTrait;

    public function setUp(): void
    {
        self::bootKernel();
    }

    /**
     * Create a http client with auth headers
     */
    protected function createClientWithCredentials($token = null): Client
    {
        $token = $token ?: $this->getToken();
        return static::createClient([], ['headers' => ['authorization' => $token]]);
    }

    /**
     * Get the Bearer token for the JWT authentication
     */
    protected function getToken($body = []): string
    {
        if ($this->token) {
            return $this->token;
        }

        //$response = static::createClient()->request('POST', '/auth/login?email=test@example.com&password=123123');
        $response = static::createClient()->request('POST', '/auth/login', ['json' => $body ?: [
          'email' => 'test@example.com',
          'password' => '123123',
        ]]);

        $this->assertResponseIsSuccessful();
        $data = json_decode($response->getContent());
        $this->token = $data->token;

        return $data->token;
    }

    /**
     * Test for the GET people API
     */
    public function testGetCars(): void
    {
        $client = $this->createClientWithCredentials();

        $response = $client->request('GET', '/cars');

        $this->assertResponseIsSuccessful();
        $this->assertResponseHeaderSame('content-type', 'application/ld+json; charset=utf-8');

        // Asserts that the returned JSON is a superset of this one
        $this->assertJsonContains([
            '@context' => '/contexts/Car',
            '@id' => '/cars',
            '@type' => 'hydra:Collection',
            'hydra:totalItems' => 101,
            'hydra:view' => [
                '@id' => '/cars?page=1',
                '@type' => 'hydra:PartialCollectionView',
                'hydra:first' => '/cars?page=1',
                'hydra:last' => '/cars?page=4',
                'hydra:next' => '/cars?page=2',
            ],
        ]);

        // Because test fixtures are automatically loaded between each test, you can assert on them
        $this->assertCount(30, $response->toArray()['hydra:member']);

        // Asserts that the returned JSON is validated by the JSON Schema generated for this resource by API Platform
        // This generated JSON Schema is also used in the OpenAPI spec!
        $this->assertMatchesResourceCollectionJsonSchema(Car::class);
    }

    public function testCreateCar(): void
    {
        $client = $this->createClientWithCredentials();

        $iri = $this->findIriBy(Person::class, ['firstName' => 'TestTestTest']);

        // Create the car
        $response = $client->request('POST', '/cars', ['json' => [
          'make' => 'Ford',
          'model' => 'Focus',
          'registrationNumber' => 'AB12CDE',
          'owner' => $iri
        ]]);
        
        $this->assertResponseIsSuccessful();
    }

    
    public function testUpdateCar(): void
    {
        $client = $this->createClientWithCredentials();

        $iri = $this->findIriBy(Car::class, ['registrationNumber' => '1']);

        // Update the car
        $response = $client->request('PUT', $iri, ['json' => [
            'make' => 'VW'
        ]]);
        
        $this->assertResponseIsSuccessful();
    }

    public function testDeleteCar(): void
    {
        $client = $this->createClientWithCredentials();

        $iri = $this->findIriBy(Car::class, ['registrationNumber' => '1']);

        // Update the car
        $response = $client->request('DELETE', $iri);
        
        $this->assertResponseIsSuccessful();
                
    }
}