<?php

//declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiProperty;
use App\Repository\PersonRepository;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=PersonRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
#[ApiResource(
  collectionOperations: [
      'get' => ['normalization_context' => ['groups' => 'get']],
      'post'
  ],
  
)]
class Person
{
    /**
     * @ORM\Id
     * @ApiProperty(identifier=true)
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="string", length=50)
     */
    private $firstName;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="string", length=50)
     */
    private $lastName;

    /**
     * @Assert\Type("\DateTimeInterface")
     * @ORM\Column(type="date", nullable=true)
     */
    private $dateOfBirth;

    /**
     * @ORM\OneToMany(targetEntity=Car::class, mappedBy="owner", cascade={"remove"})
     */
    private $ownedCars;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private $createdAt;

    /**
     * @Assert\DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    public function __construct()
    {
        $this->ownedCars = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @Groups({"get"})
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @Groups({"get"})
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @Groups({"get"})
     */
    public function getDateOfBirth(): ?\DateTimeInterface
    {
        return $this->dateOfBirth;
    }

    public function setDateOfBirth(?\DateTimeInterface $dateOfBirth): self
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    /**
     * @Groups({"get"})
     */
    public function getAge(): ?int
    {
        $age = null;
        if($this->getDateOfBirth() != null)
        {
          $today  = new DateTime('today');
          $age = $this->getDateOfBirth()->diff($today)->y;
        }

        return $age;
    }

    /**
     * @return Collection|Car[]
     * @Groups({"get"})
     */
    public function getOwnedCars(): Collection
    {
        return $this->ownedCars;
    }

    public function addOwnedCar(Car $ownedCar): self
    {
        if (!$this->ownedCars->contains($ownedCar)) {
            $this->ownedCars[] = $ownedCar;
            $ownedCar->setOwner($this);
        }

        return $this;
    }

    public function removeOwnedCar(Car $ownedCar): self
    {
        if ($this->ownedCars->removeElement($ownedCar)) {
            // set the owning side to null (unless already changed)
            if ($ownedCar->getOwner() === $this) {
                $ownedCar->setOwner(null);
            }
        }

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue(): void
    {
        $this->setCreatedAt(new \DateTimeImmutable());
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @ORM\PreUpdate
     */
    public function setUpdatedAtValue(): void
    {
        $this->setUpdatedAt(new \DateTime());
    }
}
